a modified version of ghacks user.js for firefox.

the original github repo seems down, but my modified version works just fine,
so use it if you want

features of this modified version
---
* webassembly works
* annoying letterboxing is gone
* webgl works
* privacy.resistFingerprinting is disabled since I'm using addons to prevent
browser fingerprinting

